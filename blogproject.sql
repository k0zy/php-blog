-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Sunucu sürümü: 10.4.8-MariaDB
-- PHP Sürümü: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `blogproject`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `blogpost`
--

CREATE TABLE `blogpost` (
  `id` int(11) NOT NULL,
  `header` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `scontent` varchar(255) COLLATE utf8_turkish_ci NOT NULL,
  `content` text COLLATE utf8_turkish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `blogpost`
--

INSERT INTO `blogpost` (`id`, `header`, `scontent`, `content`) VALUES
(18, 'Marsta su bulundu', 'İSKİ yetkilileri marsta su bulunduğunu açıkladı.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend, odio vitae cursus ornare, sapien justo convallis arcu, eu imperdiet nibh ante sit amet mauris. Ut at quam quis dolor hendrerit dapibus nec et dolor. Integer efficitur nibh ac dolor tristique ullamcorper. In consectetur semper purus, non dapibus urna. Curabitur nec felis vel massa aliquam ultrices sit amet non justo. Suspendisse fermentum turpis justo, a porta ex ornare ut. Vestibulum lacinia luctus venenatis. Integer vel neque vel ipsum suscipit venenatis eget sit amet libero. Curabitur aliquam ante non blandit feugiat. Mauris facilisis sollicitudin mauris quis aliquet. Nunc nulla velit, rutrum et odio ac, maximus imperdiet lacus. Morbi vehicula gravida blandit. \r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend, odio vitae cursus ornare, sapien justo convallis arcu, eu imperdiet nibh ante sit amet mauris. Ut at quam quis dolor hendrerit dapibus nec et dolor. Integer efficitur nibh ac dolor tristique ullamcorper. In consectetur semper purus, non dapibus urna. Curabitur nec felis vel massa aliquam ultrices sit amet non justo. Suspendisse fermentum turpis justo, a porta ex ornare ut. Vestibulum lacinia luctus venenatis. Integer vel neque vel ipsum suscipit venenatis eget sit amet libero. Curabitur aliquam ante non blandit feugiat. Mauris facilisis sollicitudin mauris quis aliquet. Nunc nulla velit, rutrum et odio ac, maximus imperdiet lacus. Morbi vehicula gravida blandit. '),
(19, 'Kızılay i9 işlemci dağıtacak !', 'Elinde fazla bulunan i9 işlemcileri elden çıkartmak istiyorlar.', 'Elindeki fazla i9 işlemcileri çöpe atmak istemeyen Kızılay işlemcileri ihtiyaç sahiplerine ulaştıracak. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend, odio vitae cursus ornare, sapien justo convallis arcu, eu imperdiet nibh ante sit amet mauris. Ut at quam quis dolor hendrerit dapibus nec et dolor. Integer efficitur nibh ac dolor tristique ullamcorper. In consectetur semper purus, non dapibus urna. Curabitur nec felis vel massa aliquam ultrices sit amet non justo. Suspendisse fermentum turpis justo, a porta ex ornare ut. Vestibulum lacinia luctus venenatis. Integer vel neque vel ipsum suscipit venenatis eget sit amet libero. Curabitur aliquam ante non blandit feugiat. Mauris facilisis sollicitudin mauris quis aliquet. Nunc nulla velit, rutrum et odio ac, maximus imperdiet lacus. Morbi vehicula gravida blandit. '),
(20, '500T hattı samanyolu galaksisine uzanacak.', 'İstanbul Büyükşehir Belediyesi yetkilileri 500T otobüs hattının uzunluğunun yeterli gelmediğini düşünerek bir ilke imza attı. Satürn Büyükşehir Belediyesi ile görüşülen proje belediye meclisinden onay aldı.', 'İstanbul Büyükşehir Belediyesi yetkilileri 500T otobüs hattının uzunluğunun yeterli gelmediğini düşünerek bir ilke imza attı. Satürn Büyükşehir Belediyesi ile görüşülen proje belediye meclisinden onay aldı.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eleifend, odio vitae cursus ornare, sapien justo convallis arcu, eu imperdiet nibh ante sit amet mauris. Ut at quam quis dolor hendrerit dapibus nec et dolor. Integer efficitur nibh ac dolor tristique ullamcorper. In consectetur semper purus, non dapibus urna. Curabitur nec felis vel massa aliquam ultrices sit amet non justo. Suspendisse fermentum turpis justo, a porta ex ornare ut. Vestibulum lacinia luctus venenatis. Integer vel neque vel ipsum suscipit venenatis eget sit amet libero. Curabitur aliquam ante non blandit feugiat. Mauris facilisis sollicitudin mauris quis aliquet. Nunc nulla velit, rutrum et odio ac, maximus imperdiet lacus. Morbi vehicula gravida blandit. ');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `blogpost`
--
ALTER TABLE `blogpost`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `blogpost`
--
ALTER TABLE `blogpost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
