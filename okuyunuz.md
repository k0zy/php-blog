Kodu canlı incelemek istediğiniz takdirde blogproject isimli veritabanını oluşturup bulunan SQL dosyasını import etmeniz gerekmektedir.

Post eklemek için phpblog/admin yoluna girmelisiniz.

Localhost da dosya yolunu consts.php dosyasından ayarlayınız.

/index.php dosyası sabit defineları, database classını ve index.php dosyasının frontend kısmını getirir.

/consts.php dosyasında definelar tanımlanır.

/post.php dosyası seçili postu getirir.

/admin klasörü içerisinde tema dosyaları bulunmaktadır.



substr fonksiyonu;
        admin/index.php dosyasının
                223. satırında mevcut.

GET ile çalışan form;
        admin/addpost.php dosyasının
                222. satırından itibaren mevcut.

Sağlanması gereken diğer şartlar;
        source/index.php dosyasının
                başından itibaren mevcut.