<?php


require_once __DIR__ . "/database/db.php";

class Reha
{
    
    public $uyeozellik = "";
    
    public function susaniyeninenguzelsayisi()
    {
      // rand fonksiyonu kullanarak verilen parametreler arasinda sistem random sayi uretir.
      echo "Şu saniyenin en güzel sayısı ".rand(0,1000);
    }
    public function caySoyle()
    {
      // Bu fonksiyon her cagirildiginda statik degisken tekrardan olusmaz ayni kalir.
      static $caygonder = 1;
      echo "<br>Siz o işlemi yaparken arkaplanda ".$caygonder." tane çay söylendi.";
      $caygonder = $caygonder + 1 ;
    }

}
?>
<?php
// Aldigimiz undefined index hatasini kaldirmak icin yazilmis kodlar
error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_reporting', E_ALL ^ E_NOTICE);
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PHP BLOG PROJECT</title>

  <!-- Bootstrap core CSS -->
  <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="./vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  
  
  <!-- Custom styles for this template -->
  <link href="./css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="./index.php">PHP BLOG PROJECT</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('./img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>
            PHP BLOG PROJECT

            </h1>
            <span class="subheading"><?php $nesnem1 = new Reha();
            $nesnem1->susaniyeninenguzelsayisi() ?></span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
  
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
      
        <div class="post-preview">
        <?php foreach(db::get("SELECT * FROM blogpost") as $listele)
                {?>
          <a href="./post.php?id=<?php echo $listele->id;?>">
            <h2 class="post-title">
            <?php echo $listele->header;?>
            </h2>
            <h3 class="post-subtitle">
            <?php echo $listele->scontent;?>
            </h3>
          </a>
            <?php }?>
        </div>
        <hr>
        
        Text girin hashleyelim abi
        <form action="index.php#fordsa" id="fordsa">
        <input type="text" class="form-control" name="mdfive" placeholder="Giriniz">
        <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
        </form>
        
        <?php 
        if(isset($_GET['mdfive']))
        {
          $nesnem = new Reha();
          $gelendeger = $_GET['mdfive'];
          $nesnem->uyeozellik = $gelendeger;
          echo md5($nesnem->uyeozellik);
          
        }
        $nesnem2 = new Reha();
        $nesnem2->caySoyle();
          $nesnem2->caySoyle();
          $nesnem2->caySoyle();
?>
        
      </div>
    </div>
    
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
      <?php
      // Asagidaki iliskisel arrayde gelistiricilerin isimleri tutuluyor.
              $isimler = array("Developers" => array("bir" => "Emirhan Yılmaz","iki" => "Baris Dogan"));
              echo $isimler["Developers"]["bir"];
              echo "<br>";
            echo $isimler["Developers"]["iki"];

            // SERVER DOCUMENT ROOT SUPER DEGISKENI KULLANICININ BULUNDUGU DIZINI VERIYOR
            echo "<br>Bu script ";
            echo $_SERVER['DOCUMENT_ROOT'];
            echo " dizininde çalışıyor";?>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="./vendor/jquery/jquery.min.js"></script>
  <script src="./vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="./js/clean-blog.min.js"></script>

</body>

</html>
